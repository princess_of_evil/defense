package com.cab404.defense;

import com.cab404.defense.primitives.GameObj;
import com.cab404.defense.primitives.Particle;

/**
 * Простейший класс, отвечающий за создание частиц через равные промежутки
 * времени
 * 
 * @author cab404
 * 
 */
public abstract class ParticleEmmiter {
	public float delay;
	protected float delayLeft;
	public GameObj clipped_to;


	public abstract Particle getTemplate();


	public void step(float time) {
		if (clipped_to != null) {
			delayLeft += time;
			for (; delayLeft > delay; delayLeft -= delay) {
				Particle prt = getTemplate();
				clipped_to.centerObjectOnSelf(prt);
				Defense.game_data.add(prt);
			}
		}
	}
}
