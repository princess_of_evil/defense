package com.cab404.defense;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Keys;

/**
 * Хранилище всякой всячины из файлов
 * 
 * @author cab404
 * 
 */
public class DataStorage {

	/**
	 * Используется для удобства организации загрузки и доступа к изображениям.
	 * @author cab404
	 *
	 */
	public enum Tiles {
		//@formatter:off
		TEXT_BG(1, 24, 2, 2),

		ENEMY(23, 0, 11, 13),
		TOWER(0, 0, 23, 23),
		ROCKET(0, 57, 5, 7),
		
		PARTICLE_1(0, 52,  5, 5),
		PARTICLE_2(0, 47,  5, 5),
		PARTICLE_3(0, 42, 5, 5),
		PARTICLE_4(0, 37, 5, 5),
		PARTICLE_5(0, 32, 5, 5),
		PARTICLE_6(0, 27, 5, 5),
		PARTICLE_7(5, 51, 5, 5);
		
		//@formatter:on
		public int x, y, w, h;


		Tiles(int x, int y, int w, int h) {
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
		}
	}

	// Символы, загружаемые из шрифта.
	public static final String chars =
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
					+ "abcdefghijklmnopqrstuvwxyz"
					+ "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
					+ "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
					+ "1234567890-=!\"№;%:?*()_+\\/|<>.,'[]{}";

	public static ObjectMap<String, Texture> texture_storage;
	public static ObjectMap<Tiles, Sprite> sprite_storage;
	/**
	 * Шрифт, используемый везде. Понимаю, глупо, потом добавлю что-нибудь по-интересее.
	 */
	public static BitmapFont font;


	public static void loadResourses() {
		FreeTypeFontGenerator ftf;

		ftf = new FreeTypeFontGenerator(
				Gdx.files.internal("data/ubuntu-mono.ttf"));
		font = ftf.generateFont(18, chars, false);


		texture_storage = new ObjectMap<String, Texture>();
		sprite_storage = new ObjectMap<Tiles, Sprite>();


		Texture tiles;

		tiles = new Texture(Gdx.files.internal("data/tiles.png"));
		texture_storage.put("tiles", tiles);

		for (int i = 0; i != Tiles.values().length; i++) {
			Tiles tmp;
			tmp = Tiles.values()[i];

			Sprite toAdd = new Sprite(new TextureRegion(tiles, tmp.x, tmp.y, tmp.w, tmp.h));
			sprite_storage.put(tmp, toAdd);
		}

	}


	public static void disposeResourses() {
		Keys<String> iter = texture_storage.keys();

		while (iter.hasNext()) {
			texture_storage.get(iter.next()).dispose();
		}
	}
}
