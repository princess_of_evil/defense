package com.cab404.defense;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

/**
 * Defense Input Processor
 * 
 * @author cab404
 * 
 */


public class DIP implements InputProcessor {

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}


	@Override
	public boolean keyUp(int keycode) {
		return false;
	}


	@Override
	public boolean keyTyped(char character) {
		return false;
	}


	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}


	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}


	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}


	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}


	@Override
	public boolean scrolled(int amount) {

		// Довольно просто - сохраняем положение
		// курсора до зума и после, разницу добавляем к положению камеры, и
		// вуаля - курсор при зуме всегда на одной точке.
		Vector2 mouse = MouseUtils.getGameMousePos(0);
		Defense.cam.zoom += amount * 0.1f;
		Defense.cam.zoom = Defense.cam.zoom > 0.1f ? Defense.cam.zoom : 0.1f;
		Vector2 mouse2 = MouseUtils.getGameMousePos(0);

		Defense.cam.translate(mouse.sub(mouse2));

		return true;
	}
}
