package com.cab404.defense.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.DataStorage;
import com.cab404.defense.DataStorage.Tiles;
import com.cab404.defense.Defense;
import com.cab404.defense.ParticleEmmiter;
import com.cab404.defense.primitives.GameObj;
import com.cab404.defense.primitives.Particle;
import com.cab404.defense.primitives.Ship;
import com.cab404.defense.primitives.Tower;

public class LaserTower extends Tower {

	public static class Laser extends Particle {

		public static class TraceEmmiter extends ParticleEmmiter {

			public TraceEmmiter(GameObj owner) {
				this.delay = 0.0001f;
				this.clipped_to = owner;
			}


			@Override
			public Particle getTemplate() {
				Particle prt = new Particle();
				prt.var.img = //new Sprite(
						DataStorage.sprite_storage.get(Tiles.PARTICLE_7)
						//)
						;
				prt.live = 0.5f;
				
				prt.var.way = clipped_to.var.way.cpy();

				prt.stats.color = Color.CYAN.cpy();
				prt.stats.speed = 0;
				
				return prt;
			}

		}

		public ParticleEmmiter trace;


		public Laser() {
			super();
			var.way = new Vector2((float) Math.random() - 0.5f, (float) Math.random() - 0.5f);
			trace = new TraceEmmiter(this);
		}


		@Override
		public void update() {
			move(true);
			if (var.livetime > live)
				var.hp = 0;
		}


		@Override
		protected void move_iteration(float iter) {
			trace.step(time / iter);
		}


		@Override
		public void onDeath() {
			//genExplotion(20);
		}

	}


	public LaserTower() {
		super();

		stats.firing_speed = 0.2f;
		stats.firing_range = 1500;
		
		projectile_stats.rotation_speed = 0;
		projectile_stats.speed = 50000;
		projectile_stats.damage = 5;
		projectile_stats.color = Color.CYAN.cpy();
		
		var.hp = 200;
		stats.max_hp = 200;
	}


	@Override
	public GameObj getProjectile() {
		Laser proj = new Laser();
		centerObjectOnSelf(proj);

		proj.var.target = var.target;
		proj.var.way = var.target.var.pos.cpy().sub(proj.var.pos);

		proj.stats = projectile_stats;

		return proj;
	}


	@Override
	public void update() {
		this.var.target = findNearestIn(Defense.game_data.getByClass(Ship.class));
		sendAllRockets();
		destroyFarRockets();
	}

}
