package com.cab404.defense.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.DataStorage;
import com.cab404.defense.Defense;
import com.cab404.defense.ParticleEmmiter;
import com.cab404.defense.Statistics;
import com.cab404.defense.Varibles;
import com.cab404.defense.DataStorage.Tiles;
import com.cab404.defense.primitives.GameObj;
import com.cab404.defense.primitives.Particle;
import com.cab404.defense.primitives.Ship;
import com.cab404.defense.primitives.Tower;

/**
 * Ну вcё предельно ясно. Башня, стреляющая ракетами.
 * 
 * @author cab404
 * 
 */
public class RocketTower extends Tower {

	public static class Rocket extends Particle {

		public static class TraceEmmiter extends ParticleEmmiter {

			public TraceEmmiter(GameObj owner) {
				this.delay = 0.02f;
				this.clipped_to = owner;
			}


			@Override
			public Particle getTemplate() {
				Particle prt = new Particle();
				prt.live = 0.5f;
				prt.stats.color = Color.GRAY.cpy();
				prt.var.way = clipped_to.var.way.cpy();
				prt.stats.speed = 0;
				return prt;
			}

		}

		public ParticleEmmiter trace;


		public Rocket() {
			var = new Varibles();
			stats = new Statistics();

			live = 5f;
			var.img = new Sprite(DataStorage.sprite_storage.get(Tiles.ROCKET));
			var.way = new Vector2((float) Math.random() - 0.5f, (float) Math.random() - 0.5f);
			trace = new TraceEmmiter(this);
		}


		@Override
		protected void move_iteration(float iter) {
			trace.step(time / iter);
		}


		@Override
		public void update() {
			smoothRotation();
			move(true);
			if (var.livetime > live)
				var.hp = 0;
		}


		@Override
		public void onDeath() {
			genExplotion(10);
		}

	}


	public RocketTower() {
		super();
		var.hp = 200;
		
		stats.firing_range = 500;
		stats.max_hp = 200;
		stats.speed = 5;
		stats.firing_speed = 10;
		stats.rotation_speed = 60 * 3;

		projectile_stats.speed = 300;
	    projectile_stats.rotation_speed = 60 * 6;
	    projectile_stats.color = Color.GREEN.cpy();
	}
	
	@Override
	public GameObj getProjectile(){
		Rocket proj = new Rocket();
		centerObjectOnSelf(proj);
		proj.stats = projectile_stats;

		return proj;
	}


	@Override
	public void update() {
		this.var.target = findNearestIn(Defense.game_data.getByClass(Ship.class));
		sendAllRockets();
		retargetObjects();
		destroyFarRockets();
	}


}
