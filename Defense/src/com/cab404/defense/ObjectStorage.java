package com.cab404.defense;

import java.util.Iterator;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.cab404.defense.primitives.GameObj;

/**
 * Хранилище для объектов.
 * 
 * @author cab404
 * 
 */
public class ObjectStorage {
	private ObjectMap<Class<? extends GameObj>, Array<GameObj>> objects;


	public ObjectStorage() {
		objects = new ObjectMap<Class<? extends GameObj>, Array<GameObj>>();
	}


	Package primitives = Package.getPackage("com.cab404.defense.primitives");


	@SuppressWarnings("unchecked")
	private Class<? extends GameObj> getPrimitive(GameObj obj) {
		Class<? extends GameObj> tmp = obj.getClass();

		while (!tmp.getPackage().equals(primitives))
			tmp = (Class<? extends GameObj>) tmp.getSuperclass();

		return tmp;
	}


	public void add(GameObj obj) {
		Class<? extends GameObj> obj_class = getPrimitive(obj);

		Array<GameObj> to_work_with;

		if (!objects.containsKey(obj_class)) {
			to_work_with = new Array<GameObj>();
			objects.put(obj_class, to_work_with);
			// System.out.println("Created new array for " + obj_class);
		} else {
			to_work_with = (Array<GameObj>) objects.get(obj_class);
		}


		to_work_with.add(obj);
//		System.out.println("---");
//		System.out.println("Object added into array " + obj_class);
//		System.out.println(" objects in total: " + to_work_with.size);

	}


	public Array<GameObj> getByClass(Class<? extends GameObj> clazz) {
		Array<GameObj> tmp = objects.get(clazz);
		return tmp == null ? new Array<GameObj>() : tmp;
	}


	public void remove(GameObj obj) {
		if (objects.containsKey(getPrimitive(obj))) {

			Class<? extends GameObj> obj_class = getPrimitive(obj);
			Array<GameObj> to_work_with = objects.get(obj_class);
			to_work_with.removeValue(obj, true);
//			System.out.println("---");
//			System.out.println("Object removed from array " + obj_class);
//			System.out.println(" objects in total: " + to_work_with.size);

		}
	}


	public Array<GameObj> getAll() {
		Array<GameObj> all_objects = new Array<GameObj>();

		Iterator<Array<GameObj>> iter = objects.values().iterator();

		while (iter.hasNext()) {
			all_objects.addAll(iter.next());
		}

		return all_objects;

	}


	public void update(int iterations, float time) {
		for (int i = iterations; i != 0; i--) {
			update(time / iterations);
		}
	}


	private void update(float time) {
		GameObj.time = time;
		Array<GameObj> toUpdate = getAll();
		Iterator<GameObj> update = toUpdate.iterator();

		Vector2 ui_mouse = MouseUtils.getMousePos(0);
		Vector2 game_mouse = MouseUtils.ui_to_game(MouseUtils.getMousePos(0));

		while (update.hasNext()) {
			GameObj tmp = update.next();

			if (tmp.var.hp > 0) {

				tmp.internalUpdate();

				if (tmp.var.is_ui)
					if (tmp.var.img.getBoundingRectangle().contains(ui_mouse.x, ui_mouse.y))
						tmp.onMouseOver(ui_mouse);
					else
						;
				else if (tmp.var.img.getBoundingRectangle().contains(game_mouse.x, game_mouse.y))
					tmp.onMouseOver(game_mouse);
				else
					;

				tmp.update();

			} else {

				remove(tmp);
				tmp.onDeath();
				tmp.var.isDead = true;
				update.remove();

			}
		}


	}


	public void render() {
		// Рисуем игровые объекты
		SpriteBatch gui, game;
		gui = Defense.gui_batch;
		game = Defense.game_batch;

		Iterator<GameObj> render = getAll().iterator();
		Array<GameObj> ui = new Array<GameObj>();

		game.setProjectionMatrix(Defense.cam.combined);
		game.getProjectionMatrix().translate(-Defense.w / 2 * Defense.cam.zoom,
				-Defense.h / 2 * Defense.cam.zoom, 0);

		game.begin();
		{
			while (render.hasNext()) {
				GameObj tmp = render.next();
				if (!tmp.var.is_ui)
					tmp.draw(game);
				else
					// Если UI - пихаем в отдельный массив, оттуда и будем
					// рендерить.
					ui.add(tmp);
			}
		}
		game.end();

		// Рисуем UI
		render = ui.iterator();
		gui.begin();
		{
			while (render.hasNext())
				render.next().draw(gui);

			DataStorage.font.draw(gui, "x" + String.valueOf(Defense.time_zoom), 30, 30);
		}
		gui.end();
	}
}
