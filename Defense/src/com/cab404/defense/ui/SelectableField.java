package com.cab404.defense.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class SelectableField extends TextField {

	public static abstract class SelectionEventListener {
		public abstract void onSelect(Vector2 mouse);
	}

	public SelectionEventListener listener;
	private boolean wasTouched;
	public boolean isDead = false;
	public boolean isMouseOver = false;
	public boolean fastsel = true;
	public String text = "Nothing";


	public SelectableField(SelectionEventListener sel) {
		listener = sel;
	}


	public SelectableField(SelectionEventListener sel, String text) {
		this(sel);
		this.text = text;
	}


	@Override
	public boolean isDead() {
		return isDead || (fastsel && !Gdx.input.isTouched());
	}


	@Override
	public void onMouseOver(Vector2 mouse_pos) {
		isMouseOver = true;
		bg.setColor(1, 1, 1, 0.9f);
		if (fastsel) {
			if (!Gdx.input.isTouched()) {
				listener.onSelect(mouse_pos);
			}
		} else if (Gdx.input.isTouched()) {
			wasTouched = true;
		} else if (wasTouched) {}
	}


	@Override
	public String getText() {
		return text;
	}


	@Override
	public void update() {
		if (!isMouseOver) {
			bg.setColor(1, 1, 1, 0.5f);
		} else {
			isMouseOver = false;
		}
	}

}
