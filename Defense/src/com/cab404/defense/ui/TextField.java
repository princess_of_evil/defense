package com.cab404.defense.ui;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.DataStorage;
import com.cab404.defense.DataStorage.Tiles;

public abstract class TextField implements Field {

	public abstract String getText();

	/** Расстояние от текста до края фона */
	public float frame = 5;
	public Sprite bg;


	public TextField() {
		bg = new Sprite(DataStorage.sprite_storage.get(Tiles.TEXT_BG));
		bg.setColor(1, 1, 1, 0.5f);
	}


	@Override
	public void draw(SpriteBatch batch, Vector2 lbc) {
		String txt = getText();
		TextBounds bounds = DataStorage.font.getMultiLineBounds(txt);

		bg.setPosition(lbc.x, lbc.y);
		bg.setSize(bounds.width + frame * 2, bounds.height + frame * 2);
		bg.draw(batch);

		DataStorage.font.drawMultiLine(
				batch,
				txt,
				lbc.x + frame,
				lbc.y + frame + bounds.height
				);
	}


	@Override
	public Vector2 getSize() {
		String txt = getText();
		TextBounds bounds = DataStorage.font.getMultiLineBounds(txt);
		return new Vector2(bounds.width + frame * 2, bounds.height + frame * 2);
	}


	@Override
	public void update() {}

}
