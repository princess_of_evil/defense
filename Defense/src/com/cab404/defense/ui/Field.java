package com.cab404.defense.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Интерфейс элементов меню
 * @author cab404
 *
 */
public interface Field {
	/**
	 * Рисует поле меню
	 * @param batch
	 * @param lbc Левый нижний угол
	 */
	public abstract void draw(SpriteBatch batch, Vector2 lbc);
	public abstract Vector2 getSize();
	public abstract boolean isDead();
	public abstract void update();
	public abstract void onMouseOver(Vector2 mouse_pos);
}