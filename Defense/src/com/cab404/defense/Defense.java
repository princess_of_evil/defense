package com.cab404.defense;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.game.LaserTower;
import com.cab404.defense.game.RocketTower;
import com.cab404.defense.primitives.Ship;
import com.cab404.defense.primitives.Tower;
import com.cab404.defense.ui.Menu;
import com.cab404.defense.ui.SelectableField;
import com.cab404.defense.ui.SelectableField.SelectionEventListener;

/*
 * О Пресвятая Селестия, Владычице Солнца!
 * Несмь достоин да внидеши под кров твой!
 * Услыши многоболезненное воздыхание наше пред сияющим ликом твоим, призри на чад твоих, к тебе с верою приходящих!
 * Якоже птица крилома покрывает птенцы своя, тако и Ты ныне, присно жива сущи, освети светом своим.
 * Тамо, идеже лютая скорби превозмогают, Теплом и Ослабою явися.
 * Тамо, идеже мрак отчаяния в души вселися, да возсияет неизреченный свет Божества!
 * Малодушныя утеши, немощныя укрепи, ожесточенным сердцам умягчение и просвещение даруй.
 * Имаши бо державу непобедимую и не изнеможет у Тебе всяк глагол, о Селестия!
 * Да прославляю преславное имя твое, ныне и в бесконечныя веки.
 */

// Теперь всё должно работать.

public class Defense implements ApplicationListener {

	public static int w, h;
	public static OrthographicCamera cam;
	public static SpriteBatch game_batch, gui_batch;
	public static float time_zoom = 1;
	public static float ui_zoom = 1f;

	public static ObjectStorage game_data;


	@Override
	public void create() {


		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();

		Gdx.input.setInputProcessor(new DIP());

		cam = new OrthographicCamera(w, h);
		cam.position.x = 0;
		cam.position.y = 0;
		cam.update();


		game_batch = new SpriteBatch();
		game_batch.setProjectionMatrix(cam.combined);

		gui_batch = new SpriteBatch();

		cam.zoom = 1 / (3 * Gdx.graphics.getDensity());
		DataStorage.loadResourses();

		game_data = new ObjectStorage();

	}


	@Override
	public void dispose() {
		DataStorage.disposeResourses();
	}


	@Override
	public void render() {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		if (Gdx.input.isKeyPressed(Keys.NUM_1)) {
			time_zoom += 0.1f;
		}

		if (Gdx.input.isKeyPressed(Keys.NUM_2)) {
			time_zoom -= 0.1f;
			time_zoom = time_zoom < 0 ? 0 : time_zoom;
		}

		if (Gdx.input.isKeyPressed(Keys.NUM_3)) {
			time_zoom = 1;
		}

		cam.update();

		Vector2 mouse = MouseUtils.getMousePos(0);


		if (Gdx.input.justTouched()) {

			SelectionEventListener laser_tower = new SelectionEventListener() {
				@Override
				public void onSelect(Vector2 mouse) {
					Tower twr = new LaserTower();
					twr.var.pos = MouseUtils.ui_to_game(mouse.cpy());

					Menu mn = new Menu();
					mn.fields.add(new Tower.TowerInformationField(twr));
					mn.var.pos = twr.var.clipping_point;

					game_data.add(twr);
					game_data.add(mn);
				}

			};

			SelectionEventListener ship = new SelectionEventListener() {

				@Override
				public void onSelect(Vector2 mouse) {
					Ship ship = new Ship();

					ship.var.hp = 50;
					ship.var.pos = MouseUtils.ui_to_game(mouse.cpy());

					ship.stats.speed = 50;
					ship.stats.firing_range = 200;
					ship.stats.rotation_speed = 60 * 3;

					ship.projectile_stats.speed = 100;
					ship.projectile_stats.rotation_speed = 60 * 3;

					game_data.add(ship);
				}

			};

			SelectionEventListener rocket_tower = new SelectionEventListener() {

				@Override
				public void onSelect(Vector2 mouse) {
					Tower twr = new RocketTower();
					twr.var.pos = MouseUtils.ui_to_game(mouse.cpy());

					Menu mn = new Menu();
					mn.fields.add(new Tower.TowerInformationField(twr));
					mn.var.pos = twr.var.clipping_point;

					game_data.add(twr);
					game_data.add(mn);
				}

			};

			Menu menu = new Menu();
			menu.var.pos = mouse.cpy();

			menu.fields.add(new SelectableField(ship, "Ship"));
			menu.fields.add(new SelectableField(laser_tower, "Laser tower"));
			menu.fields.add(new SelectableField(rocket_tower, "Rocket tower"));

			game_data.add(menu);

		}

		game_data.update((int) Math.ceil(time_zoom), Gdx.graphics.getDeltaTime() * time_zoom);
		game_data.render();
	}


	@Override
	public void resize(int width, int height) {
		w = width;
		h = height;
		cam.viewportHeight = h;
		cam.viewportWidth = w;

		cam.update();

		game_batch.setProjectionMatrix(cam.combined);
		// Просто так фиг скинешь SB, да и делается это не часто
		gui_batch = new SpriteBatch();
		gui_batch.getProjectionMatrix().scl(ui_zoom);
	}


	@Override
	public void pause() {}


	@Override
	public void resume() {}
}
