package com.cab404.defense;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

/**
 * Содержит различные shortcut-ы для преобразования координат
 * 
 * @author cab404
 * 
 */
public class MouseUtils {

	public static Vector2 ui_to_game(Vector2 coords) {
		return coords.cpy()
				.mul(Defense.cam.zoom * Defense.ui_zoom)
				.add(Defense.cam.position.x, Defense.cam.position.y);
	}


	public static Vector2 game_to_ui(Vector2 coords) {
		return coords.cpy()
				.sub(Defense.cam.position.x, Defense.cam.position.y)
				.div(Defense.cam.zoom * Defense.ui_zoom);
	}


	public static Vector2 getGameMousePos(int point) {

		return ui_to_game(getMousePos(point));

	}


	public static Vector2 getMousePos(int point) {

		return new Vector2(Gdx.input.getX(point), Defense.h - Gdx.input.getY(point)).div(Defense.ui_zoom);

	}

}
