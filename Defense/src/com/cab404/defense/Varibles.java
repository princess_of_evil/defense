package com.cab404.defense;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.cab404.defense.primitives.GameObj;

public class Varibles {

	// Местоположение объекта.
	public Vector2 pos;

	// Спрайт объекта. Возможно, потом заменю на анимацию из PonyGame.
	public Sprite img;

	// Направление объекта. Не влияет на скорость, см stats.speed.
	public Vector2 way;

	// Цель, которую объект атакует.
	public GameObj target;

	// Если true. то объект рисуется в gui_batch, т.е не скалируется с
	// остальными
	public boolean is_ui = false;

	// Созданные объектом ракеты. Или что либо еще.
	public Array<GameObj> createdObjects;

	// Сколько секунд живёт объект.
	public float livetime;

	// Nuff said
	public int hp = 20;

	// Сколько секунд прошло с момента последнего вызова sendAllRockets
	public float gun_cooldown = 0;

	// Nuff said
	public boolean isDead = false;

	public Vector2 clipping_point;

	public Varibles() {
		pos = new Vector2();
		way = new Vector2();
		img = new Sprite();
		clipping_point = new Vector2();
		createdObjects = new Array<GameObj>();
	}


	/**
	 * @return пол размера img
	 */
	public Vector2 halfSize() {
		return new Vector2(img.getWidth() / 2, img.getHeight() / 2);
	}
	
	public Vector2 size(){
		return new Vector2(img.getWidth(), img.getHeight());
	}


	public void draw() {
		img.setPosition(pos.x, pos.y);
		img.draw(is_ui ? Defense.gui_batch : Defense.game_batch);
	}

}
