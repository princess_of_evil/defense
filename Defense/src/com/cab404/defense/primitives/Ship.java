package com.cab404.defense.primitives;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.DataStorage;
import com.cab404.defense.Defense;
import com.cab404.defense.Statistics;
import com.cab404.defense.DataStorage.Tiles;
import com.cab404.defense.game.RocketTower;


public class Ship extends GameObj {
	
	public Statistics projectile_stats;
	int cooldown = 0;


	public Ship() {
		super();
		projectile_stats = new Statistics();
		projectile_stats.color = Color.RED.cpy();
		var.img = new Sprite(DataStorage.sprite_storage.get(Tiles.ENEMY));
		var.way = new Vector2(1, 1);
	}


	@Override
	public void draw(SpriteBatch batch) {
		var.img.setRotation(var.way.angle() - 90);
		var.draw();
	}


	@Override
	public void update() {
		var.target = findNearestIn(Defense.game_data.getByClass(Tower.class));
		smoothRotation();
		move(false);
		sendAllRockets();
		destroyFarRockets();
	}


	@Override
	public GameObj getProjectile() {
		GameObj proj = new RocketTower.Rocket();
		centerObjectOnSelf(proj);
		
		proj.var.target = var.target;
		proj.stats = projectile_stats;
		
		return proj;
	}
	
	@Override
	public void onDeath(){
		genExplotion(10);
	}

}
