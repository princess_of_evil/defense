package com.cab404.defense.primitives;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.cab404.defense.DataStorage;
import com.cab404.defense.Defense;
import com.cab404.defense.DataStorage.Tiles;
import com.cab404.defense.Statistics;
import com.cab404.defense.ui.TextField;

public abstract class Tower extends GameObj {
	protected Array<GameObj> created_objects;
	public Statistics projectile_stats;

	public static class TowerInformationField extends TextField {
		Tower tower;


		public TowerInformationField(Tower source) {
			super();
			tower = source;
		}


		@Override
		public void onMouseOver(Vector2 mouse_pos) {
		}


		@Override
		public String getText() {
			return "HP: " + tower.var.hp + "/" + tower.stats.max_hp + "\n";
		}


		@Override
		public void update() {
		}


		@Override
		public boolean isDead() {
			return tower.var.isDead;
		}

	}


	public Tower() {
		super();
		projectile_stats = new Statistics();
		created_objects = new Array<GameObj>();
		var.img = new Sprite(DataStorage.sprite_storage.get(Tiles.TOWER));
	}


	@Override
	public void draw(SpriteBatch batch) {
		var.draw();
	}


	// Всякие переменные для метода ниже
	private float curAngle = 0;
	private float anglePrecision = 0.7f;


	/**
	 * Скажу прямо - классный круг из частиц, обозначающий дальность башни.
	 */
	public void drawRangeCircle() {

		Vector2 range = new Vector2(0, stats.firing_range);
		range.rotate(curAngle);

		// Количество орбов (движущихся точек, на которых создаются частицы)
		int orbs = (int) (stats.firing_range * Math.PI / 500);

		for (int i = 0; i != orbs; i++) {
			Particle prt = new Particle();
			prt.live = 2f * 60 * time;
			prt.var.pos = range.cpy().add(var.pos);
			prt.stats.color = projectile_stats.color.cpy();
			Defense.game_data.add(prt);

			range.rotate(360 / orbs);
		}

		curAngle += anglePrecision * 500 / stats.firing_range;
	}


	@Override
	public void onMouseOver(Vector2 mouse) {
//		drawRangeCircle();
	}


	@Override
	public abstract void update();


	@Override
	public void onDeath() {
		for (int i = 1; i != 500; i++) {
			Particle prt = new Particle();

			prt.stats.color = Math.random() > 0.5 ?
					Color.GREEN.cpy() : Color.BLUE.cpy();

			prt.live = (float) (4 + (Math.random() - 0.5f) * 1f);
			prt.var.pos = var.pos.cpy().add(var.halfSize());
			prt.stats.speed = 500;

			prt.var.way = new Vector2
					((float) Math.random() - 0.5f, (float) Math.random() - 0.5f);


			Defense.game_data.add(prt);
		}
	}

}
