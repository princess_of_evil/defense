package com.cab404.defense.primitives;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.cab404.defense.DataStorage;
import com.cab404.defense.Statistics;
import com.cab404.defense.Varibles;

public class Particle extends GameObj {

	public float live = 1;

	public Particle() {
		super();
		int rnd_num = (int) (Math.random() * 6) + 1;
		var = new Varibles();
		stats = new Statistics();
		var.img = //new Sprite(
				DataStorage.sprite_storage.get(
						DataStorage.Tiles.valueOf("PARTICLE_" + rnd_num)
						);
				//);
		stats.speed = 60;
	}


	@Override
	public void draw(SpriteBatch batch) {
		var.img.setRotation(var.way.angle() - 90);
		var.img.setColor(stats.color);
		var.draw();
	}


	@Override
	public void update() {

		if (live <= var.livetime)
			var.hp = 0;
		else
			stats.color.a = (live - var.livetime) / live;
		var.img.setColor(stats.color);
		move(false);

	}


	@Override
	public void onDeath() {

	}

}
